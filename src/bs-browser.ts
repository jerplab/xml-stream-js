/** Setting up text decoder for node */
import { BS } from './bs-raw'
import { TextEncoder, TextDecoder } from 'util'
BS.setupEncodeDecoder(new TextEncoder(), new TextDecoder())
export { BS }

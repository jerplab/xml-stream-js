import { Token } from '../token'
import { Parser } from './'

/**
 * Representation of an xml element
 * @beta
 *
 */
export class XmlElement {
  constructor(readonly startTag: Token.StartTag) {}
  /**
   * {@inheritDoc Token.StartTag.getAttribute}
   */
  getAttribute(name: string) {
    return this.startTag.getAttribute(name)
  }
  /** see {@link Token.StartTag.tagName} */
  get tagName() {
    return this.startTag.tagName
  }
  /** Children */
  children: (XmlElement | Token.Text | Token.CDATA)[] = []
  /** string as xml */
  toString() {
    return this.startTag.selfClosing
      ? this.startTag.toString()
      : `${this.startTag}${this.children.length ? this.children.join('') : ''}</${this.startTag.tagName}>`
  }
}

/**
 * Parser producing a representation of an xml element see class `XmlElement`
 * @beta
 */
export const XmlElementParser = (): Parser.IParser => {
  const parser: Parser.IParser = {
    onStart(startTag, parent) {
      const element = new XmlElement(startTag)
      parent.children.push(element)
      return element
    },
    onText(text, ctx) {
      ctx.children.push(text)
    },
    onChild() {
      return this
    },
  }
  return {
    onStart: (startTag: Token.StartTag) => new XmlElement(startTag),
    onEnd(ctx: XmlElement) {
      return ctx
    },
    onText: parser.onText,
    onChild() {
      return parser
    },
  }
}

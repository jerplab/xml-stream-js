import { BS } from './bs-raw'
BS.setupEncodeDecoder(
  {
    encode(s) {
      return BS.create(Buffer.from(s || ''))
    },
  },
  {
    decode(bs) {
      return Buffer.from(bs).toString()
    },
  }
)
/** @public */
export { BS }

/**
 * @packageDocumentation
 * A simple js parser for XML for nodejs and the browser.
 *
 * It will handle smoothly line-breaks, spaces, chunks (from stream), namespaces...
 *
 * It is fast and optimized for Buffer or Uint8Array of encoded strings (utf8) but will take strings too.
 */
export { Tokenizer } from './tokenizer'
export { DocumentParser } from './document-parser'
export { Parser } from './parser'
export { Token } from './token'
// export { BS } from './bs'

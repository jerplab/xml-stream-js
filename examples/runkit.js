const { Tokenizer, DocumentParser, Parser } = require('@jerp/xml-stream-js')
const docParser = new DocumentParser(new Tokenizer())
// parsing all <a> under <root>
docParser.on('root/a', Parser.XmlElementParser() /* a parser preserving child node order */)
docParser.write('<root><a id="first">some text</a><b>ignored</b><a id="second">more text</a></root>')
const as = []
let a
while ((a = docParser.next()) /* gets next 'a' */) {
  console.log({
    tagName: a.tagName,
    id: a.getAttribute('id'),
  })
  as.push(a)
}
as.length

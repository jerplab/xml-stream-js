const { readdir, createReadStream, writeFile } = require('fs-extra')
const { createInterface } = require('readline')
const { join, parse } = require('path')
const { exec } = require('child_process')
// This script is not part of faast.js, but rather a tool to rewrite some parts
// of the generated docs from api-generator and api-documenter so they work with
// the website generated by docusaurus.
async function main() {
  const dir = './website/docs/api'
  await new Promise((resolve, reject) =>
    exec('npx @microsoft/api-extractor run --local --verbose', (err, stdout, stderr) => {
      console.log(stdout)
      console.error(stderr)
      if (err) {
        reject(err)
      } else {
        exec(`npx @microsoft/api-documenter markdown -i ./temp -o ${dir}`, (err, stdout, stderr) => {
          console.log(stdout)
          console.error(stderr)
          if (err) {
            reject(err)
          } else {
            resolve()
          }
        })
      }
    })
  )
  const docFiles = await readdir(dir)
  const apiIndex = []
  for (const docFile of docFiles) {
    try {
      const { name: id, ext } = parse(docFile)
      if (id === 'index' || ext !== '.md') {
        continue
      }
      // index just top elements
      // if (!/\./.test(id))
      apiIndex.push(id)
      const docPath = join(dir, docFile)
      const input = createReadStream(docPath)
      const output = []
      const lines = createInterface({
        input,
        crlfDelay: Infinity,
      })
      let title = ''
      lines.on('line', line => {
        let skip = false
        if (!title) {
          const titleLine = line.match(/## (.*)/)
          if (titleLine) {
            title = titleLine[1]
          }
        }
        // Docusorus does not present nicely inline <code>..</code>
        if (!/&#124;/.test(line))
          line = line.replace(
            /<code>(.*?)<\/code>/g,
            (_, b) =>
              `\`${b.replace(/&(.*?);/g, (_, e) => {
                switch (e) {
                  case 'gt':
                    return '>'
                  case 'lt':
                    return '<'
                  case 'quot':
                    return '"'
                  case 'apos':
                    return "'"
                  case '#124':
                    return '|'
                }
              })}\``
          )
        const homeLink = line.match(/\[Home\]\(.\/index\.md\) &gt; (.*)/)
        if (homeLink) {
          // Skip the breadcrumb for the toplevel index file.
          if (id !== 'faastjs') {
            output.push(homeLink[1])
          }
          skip = true
        }
        if (!skip) {
          output.push(line)
        }
      })
      await new Promise(resolve => lines.once('close', resolve))
      input.close()
      const header = ['---', `id: ${id}`, `title: ${title}`, `hide_title: true`, '---']
      await writeFile(docPath, header.concat(output).join('\n'))
    } catch (err) {
      console.error(`Could not process ${docFile}: ${err}`)
    }
  }
  await writeFile(join(dir, 'api-index.json'), JSON.stringify(apiIndex, null, '  '))
}
main()

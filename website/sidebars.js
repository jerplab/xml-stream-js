/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const apiIndex = require('./docs/api/api-index.json')
apiIndex.sort()
const topItems = []
const otherItems = []
apiIndex.sort().forEach(item => {
  const ns = item.split('.')
  switch (ns.length) {
    case 1:
      return topItems.push('api/' + item)
    case 2:
      return topItems.push('api/' + item)
    default:
      return otherItems.push('api/' + item)
  }
})
topItems.push({
  type: 'category',
  label: 'Details',
  items: otherItems,
})
module.exports = {
  someSidebar: {
    Home: ['README', 'CHANGELOG'],
    'xml-stream-js': topItems,
  },
}

module.exports = {
  title: 'xml-stream-js',
  tagline: 'A simple js parser for XML for nodejs and the browser',
  url: 'https://www.npmjs.com/search?q=%40jerp',
  baseUrl: '/xml-stream-js/',
  favicon: 'img/favicon.ico',
  organizationName: 'jerome.proost',
  projectName: 'xml-stream-js',
  themeConfig: {
    navbar: {
      title: 'xml-stream-js',
      logo: {
        alt: '@jerp',
        src: 'img/logo.jpeg',
      },
      links: [
        { to: 'docs/README', label: 'Read me', position: 'left' },
        { to: 'docs/api/xml-stream-js', label: 'API', position: 'left' },
        {
          href: 'https://www.npmjs.com/package/@jerp/xml-stream-js',
          label: 'npm',
          position: 'right',
        },
        {
          href: 'https://gitlab.com/jerplab/xml-stream-js/',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Contact',
          items: [
            {
              label: 'GitLab',
              href: 'https://gitlab.com/jerplab/xml-stream-js/pages',
            },
            {
              label: 'npm',
              href: 'https://www.npmjs.com/search?q=%40jerp',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/jeromeproost',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Jerome Proost - Built with Docusaurus.`,
    },
  },
  themes: ['@docusaurus/theme-live-codeblock'],
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  scripts: ['https://embed.runkit.com', 'https://cdn.jsdelivr.net/npm/@jerp/xml-stream-js/browser/index.amd.min.js'],
}

import React from 'react'
import classnames from 'classnames'
import Layout from '@theme/Layout'
import Link from '@docusaurus/Link'
import useDocusaurusContext from '@docusaurus/useDocusaurusContext'
import useBaseUrl from '@docusaurus/useBaseUrl'
import styles from './styles.module.css'
import { EmbedRunkit } from '../lib/runkit-component'

const features = [
  {
    title: <>Parsing XML</>,
    imageUrl: 'img//xml.svg',
    description: (
      <>
        XML large or small but built for large ones. <em>Not built for HTML scrapping.</em>
      </>
    ),
  },
  {
    title: <>into Javascript objects</>,
    imageUrl: 'img//js.svg',
    description: (
      <>
        Use a simple parsers provided by default or build your own. Need somthing trickier, use the tokenizer, it lets
        you acts each time a tag or text is emmitted (or a comment).
      </>
    ),
  },
  {
    title: <>using stream</>,
    imageUrl: 'img//stream.svg',
    description: <>Processing chunks of xml. No need to load the full xml in memory.</>,
  },
]

function Feature({ imageUrl, title, description }) {
  const imgUrl = useBaseUrl(imageUrl)
  return (
    <div className={classnames('col col--4', styles.feature)}>
      {imgUrl && (
        <div>
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  )
}

function Home() {
  const context = useDocusaurusContext()
  const { siteConfig = {} } = context
  return (
    <Layout title={`${siteConfig.title}`} description="tagline">
      <header className={classnames('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={classnames('button button--outline button--secondary button--lg', styles.getStarted)}
              to={useBaseUrl('docs/README')}
            >
              Get Started
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
        <section className={styles.embededrunkit}>
          <EmbedRunkit
            source={`const { Tokenizer, DocumentParser, Parser } = require("@jerp/xml-stream-js")
const docParser = new DocumentParser(new Tokenizer())
// parsing all <a> under <root>
docParser.on('root/a', Parser.XmlElementParser() /* a parser preserving child node order */)
docParser.write('<root><a id="first">some text</a><b>ignored</b><a id="second">more text</a></root>')
const as = []
let a
while (a = docParser.next() /* gets next 'a' */) {
  console.log({
    tagName: a.tagName,
    id: a.getAttribute('id')
  })
  as.push(a)
}
as.length`}
          />
        </section>
      </main>
    </Layout>
  )
}

export default Home

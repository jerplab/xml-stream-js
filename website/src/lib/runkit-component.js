const React = require('react')
const PropTypes = require('prop-types')

// Based off code from https://github.com/runkitdev/react-runkit

export class EmbedRunkit extends React.Component {
  shouldComponentUpdate() {
    return false
  }
  componentWillReceiveProps(nextProps) {
    if (this.embed) {
      const { props } = this
      if (props.source !== nextProps.source) this.embed.setSource(nextProps.source)
      if (props.preamble !== nextProps.preamble) this.embed.setPreamble(nextProps.preamble)
    }
  }
  evaluate(callback) {
    this.embed.evaluate(callback)
  }
  getSource(callback) {
    this.embed.getSource(callback)
  }
  getURL() {
    return this.embed.URL
  }
  componentDidMount() {
    const element = this.refs.embed
    const options = { ...this.props, element }

    if (RunKit) this.embed = RunKit.createNotebook(options)
  }
  componentWillUnmount() {
    this.embed.destroy()
    this.embed = null
  }
  render() {
    return <div ref="embed" className={this.props.className || 'react-runkit'} />
  }
}

EmbedRunkit.source = PropTypes.string
EmbedRunkit.readOnly = PropTypes.bool
EmbedRunkit.mode = PropTypes.string
EmbedRunkit.nodeVersion = PropTypes.string
EmbedRunkit.env = PropTypes.array
EmbedRunkit.title = PropTypes.string
EmbedRunkit.minHeight = PropTypes.string
EmbedRunkit.packageTimestamp = PropTypes.string
EmbedRunkit.preamble = PropTypes.string
EmbedRunkit.onLoad = PropTypes.func
EmbedRunkit.onURLChanged = PropTypes.func
EmbedRunkit.onEvaluate = PropTypes.func

## [1.1.8](https://gitlab.com/jerplab/xml-stream-js/compare/v1.1.7...v1.1.8) (2020-01-24)


### Bug Fixes

* docs ([365c203](https://gitlab.com/jerplab/xml-stream-js/commit/365c203c44c7b2d126d45fcb083e28f78ece742a))

## [1.1.7](https://gitlab.com/jerplab/xml-stream-js/compare/v1.1.6...v1.1.7) (2020-01-24)


### Bug Fixes

* skipping the current node including the end-tag ([af855dc](https://gitlab.com/jerplab/xml-stream-js/commit/af855dcfbd246ff4b46b5abf13416e37f846fb73))

## [1.1.6](https://gitlab.com/jerplab/xml-stream-js/compare/v1.1.5...v1.1.6) (2020-01-14)

### Bug Fixes

- skips closing tag correctly
  ([3b3e62f](https://gitlab.com/jerplab/xml-stream-js/commit/3b3e62f5f683f7585d1c464d07ca6c469d0c3db1)), closes
  [#1](https://gitlab.com/jerplab/xml-stream-js/issues/1)

## [1.1.5](https://gitlab.com/jerplab/xml-stream-js/compare/v1.1.4...v1.1.5) (2020-01-14)

### Bug Fixes

- builds amd for the browser
  ([65441d4](https://gitlab.com/jerplab/xml-stream-js/commit/65441d4c83f4940c9b0017ea3e90f3e254061dbd))

## [1.1.4](https://gitlab.com/jerplab/xml-stream-js/compare/v1.1.3...v1.1.4) (2020-01-14)

### Bug Fixes

- adds missing artifacts
  ([3b27495](https://gitlab.com/jerplab/xml-stream-js/commit/3b2749513a708abf979de0421b7051df3151d667))

## [1.1.3](https://gitlab.com/jerplab/xml-stream-js/compare/v1.1.2...v1.1.3) (2020-01-14)

### Bug Fixes

- jsdelivr entry point
  ([5c56672](https://gitlab.com/jerplab/xml-stream-js/commit/5c56672204051abaaba41cd661b460d400046275))

## [1.1.2](https://gitlab.com/jerplab/xml-stream-js/compare/v1.1.1...v1.1.2) (2020-01-14)

### Bug Fixes

- build umd file ([6936dba](https://gitlab.com/jerplab/xml-stream-js/commit/6936dba71cfef0f757fe8f9ad92713fba1ac8e40))

## [1.1.1](https://gitlab.com/jerplab/xml-stream-js/compare/v1.1.0...v1.1.1) (2020-01-14)

### Bug Fixes

- fix readme ([8796a7e](https://gitlab.com/jerplab/xml-stream-js/commit/8796a7e7b87b193391a75574ff81ec566ca277f9))

# [1.1.0](https://gitlab.com/jerplab/xml-stream-js/compare/v1.0.4...v1.1.0) (2020-01-14)

### Bug Fixes

- fixes test script
  ([9385cad](https://gitlab.com/jerplab/xml-stream-js/commit/9385cad545c9d35fe15210644381e7f412f8283b))

### Features

- parses tokens ([fc1e83a](https://gitlab.com/jerplab/xml-stream-js/commit/fc1e83a5d073e062957048f8b1563b72706cf236))

## [1.0.4](https://gitlab.com/jerplab/xml-stream-js/compare/v1.0.3...v1.0.4) (2020-01-08)

### Bug Fixes

- fine tune readme sample
  ([0d7d02b](https://gitlab.com/jerplab/xml-stream-js/commit/0d7d02b976581df70c63403117ca929505343374))

## [1.0.3](https://gitlab.com/jerplab/xml-stream-js/compare/v1.0.2...v1.0.3) (2020-01-07)

### Bug Fixes

- make readme example work
  ([2ac4f7b](https://gitlab.com/jerplab/xml-stream-js/commit/2ac4f7b7fcd03b4308bc4349ac786e70a04351b4))

## [1.0.2](https://gitlab.com/jerplab/xml-stream-js/compare/v1.0.1...v1.0.2) (2020-01-07)

### Bug Fixes

- set access to public
  ([b4d462c](https://gitlab.com/jerplab/xml-stream-js/commit/b4d462cbe10abef534410a4fa1b8c8c958adfd4e))

## [1.0.1](https://gitlab.com/jerplab/xml-stream-js/compare/v1.0.0...v1.0.1) (2020-01-07)

### Bug Fixes

- cleanup npm release
  ([12f8218](https://gitlab.com/jerplab/xml-stream-js/commit/12f821851ae35e376cb133c048d9ea58e9a65fb1))

# 1.0.0 (2020-01-07)

### Features

- **tokenizer:** tokenizer
  ([15f5024](https://gitlab.com/jerplab/xml-stream-js/commit/15f50240c5bb8eabf9e387bc7cf7b330a7f31226))

import { BS } from '../src/bs-raw'
import { TextEncoder, TextDecoder } from 'util'

describe('Buffers Utils inititalization', () => {
  it('throw when not initialized ', () => {
    expect(() => BS.encode()).toThrow()
    expect(() => BS.decode()).toThrow()
  })
  it('initialized with Text(En/De)coder', () => {
    BS.setupEncodeDecoder(new TextEncoder(), new TextDecoder())
    expect(BS.encode()).toBeDefined()
  })
  it('initialized with Text(En/De)coder', () => {
    BS.setupEncodeDecoder(
      {
        encode(s) {
          return BS.create(Buffer.from(s || ''))
        },
      },
      {
        decode(bs) {
          return Buffer.from(bs).toString()
        },
      }
    )
    expect(BS.encode()).toBeDefined()
  })
})

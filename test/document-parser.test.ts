import { Tokenizer, DocumentParser, Parser } from '../src/'

describe('Pasering XML document', () => {
  it('parse xml `asis` preserving order', () => {
    const a0String = '<a id="a0"><aa/>some text a0<ab><abChild/></ab>more text</a>'
    const a1String = '<a id="a1"><aa/>some text a1<ab/><ab></ab>more text</a>'
    const b0String = '<b id="b0">some text b0</b>'
    const docParser = new DocumentParser(new Tokenizer())
    docParser.on('root/a', Parser.XmlElementParser())
    docParser.on('root/b', Parser.XmlElementParser())
    docParser.on('root/c', Parser.XmlElementParser())
    docParser.write(`<root>${a0String}${a1String}${b0String}</root>`)
    const a0 = docParser.next() as Parser.XmlElement
    const a1 = docParser.next() as Parser.XmlElement
    const b = docParser.next() as Parser.XmlElement
    const u = docParser.next() as Parser.XmlElement
    expect(a0).toBeDefined()
    expect(a1).toBeDefined()
    expect(b).toBeDefined()
    expect(u).not.toBeDefined()
    expect(a0.tagName).toBe('a')
    expect(a0.getAttribute('id')).toBe('a0')
    expect(a0.toString()).toBe(a0String)
    expect(a1.getAttribute('id')).toBe('a1')
    expect(a1.toString()).toBe(a1String)
    expect(b.getAttribute('id')).toBe('b0')
    expect(b.toString()).toBe(b0String)
  })
  it('parse whole xml document', () => {
    const aString = '<a id="a0"><aa/>some text a0<ab><abChild/></ab>more text</a>'
    const docParser = new DocumentParser(new Tokenizer())
    docParser.onRoot(Parser.XmlElementParser())
    docParser.write(aString)
    const a = docParser.next() as Parser.XmlElement
    expect(a.toString()).toBe(aString)
  })
  it('parse xml into object', () => {
    const a0String = '<a id="a0"><aa id="aa0"/><aa id="aa1"/>some text a0<ab><abChild/></ab> &amp; more text</a>'
    const a1String = '<a id="a1"><aa/>some text a1</a>'
    const b0String = '<b id="b0">some text b0</b>'
    const docParser = new DocumentParser(new Tokenizer())
    docParser.on('root/a', Parser.getXmlToObjectParser())
    docParser.on('root/b', Parser.XmlElementParser())
    docParser.write(`<root>${a0String}${a1String}${b0String}</root>`)
    const a0 = docParser.next() as any
    const a1 = docParser.next() as any
    const b = docParser.next() as Parser.XmlElement
    const u = docParser.next() as Parser.XmlElement
    expect(a0).toBeDefined()
    expect(a1).toBeDefined()
    expect(b).toBeDefined()
    expect(u).not.toBeDefined()
    expect(a0.id).toBe('a0')
    const aa0 = a0.aa[0]
    expect(aa0).toBeDefined()
    expect(aa0.id).toBe('aa0')
    expect(a0['#text']).toBe('some text a0 & more text')
    expect(a1.id).toBe('a1')
    expect(b.getAttribute('id')).toBe('b0')
    expect(b.toString()).toBe(b0String)
  })
  it('parse some elements', () => {
    const docParser = new DocumentParser(new Tokenizer())
    docParser.on('root/a', Parser.XmlElementParser())
    docParser.write(
      '<root id="a0">some<b>text</b><c/><a>some text a0</a>to ignore<c/><b>more</b><a>some text a1</a></root>'
    )
    const a0 = docParser.next() as Parser.XmlElement
    const a1 = docParser.next() as Parser.XmlElement
    const u = docParser.next()
    expect(a0).toBeDefined()
    expect(a0.toString()).toBe('<a>some text a0</a>')
    expect(a1).toBeDefined()
    expect(a1.toString()).toBe('<a>some text a1</a>')
    expect(u).not.toBeDefined()
  })
  it('parse special cases selfclosing', () => {
    const docParser = new DocumentParser(new Tokenizer())
    docParser.on('root/ab', Parser.XmlElementParser())
    docParser.write('<root id="a0"><a/>some text a0<ab>some text ab0<abChild/></ab><ab/>more text </root>')
    const ab0 = docParser.next() as Parser.XmlElement
    const ab1 = docParser.next() as Parser.XmlElement
    const u = docParser.next()
    expect(ab0).toBeDefined()
    expect(ab1).toBeDefined()
    expect(u).not.toBeDefined()
  })
  it('parse xml with custom parser', () => {
    const a0String =
      '<a id="a0"><aa id="aa00" label="item aa00"/><aa id="aa01" label="item aa01"/>  some text a0  <ab name="ab00" label="item ab00"><ignore>this is ignored</ignored> &amp; more text</a>'
    const a1String = '<a id="a1"><aa id="aa00"/>some text a1 </a>'
    const b0String = '<b id="b0">some text b0</b>'
    const parser: Parser.IParser = {
      onStart(startTag, _) {
        return {
          name: startTag.getAttribute('id'),
          items: [],
        }
      },
      onText(text, a) {
        if (!a.firstText) a.firstText = text.textContent.trim()
      },
      onEnd: (a: any) => a,
      onChild(startTag) {
        switch (startTag.tagName) {
          case 'aa': {
            return {
              onStart(startTag, parentCtx) {
                parentCtx.items.push({
                  name: startTag.getAttribute('id'),
                  label: startTag.getAttribute('label'),
                  type: 'aa',
                })
                return false
              },
            }
          }
          case 'ab': {
            return {
              onStart(startTag, parentCtx) {
                parentCtx.items.push({
                  name: startTag.getAttribute('name'),
                  label: startTag.getAttribute('label'),
                  type: 'ab',
                })
              },
              skipChildNodes: true,
            }
          }
          default:
            return false
        }
      },
    }
    const docParser = new DocumentParser(new Tokenizer())
    docParser.on('root/a', parser)
    docParser.write(`<root>${a0String}${a1String}${b0String}</root>`)
    const a0 = docParser.next() as any
    const a1 = docParser.next() as any
    const u = docParser.next() as Parser.XmlElement
    expect(a0).toMatchObject({
      name: 'a0',
      firstText: 'some text a0',
      items: [
        { type: 'aa', name: 'aa00', label: 'item aa00' },
        { type: 'aa', name: 'aa01', label: 'item aa01' },
        { type: 'ab', name: 'ab00', label: 'item ab00' },
      ],
    })
    expect(a1).toBeDefined()
    expect(u).not.toBeDefined()
  })
})

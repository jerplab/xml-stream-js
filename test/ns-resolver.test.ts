import { BS } from '../src/bs'
import { NsResolver } from '../src/ns-resolver'

describe('NSResolver', () => {
  it('creates', () => {
    const nsResolver = NsResolver.create({
      abc: 'uri://abc',
      xyz: 'uri://xyz',
    })
    expect(nsResolver.length).toBe(2)
    const abc = nsResolver[0]
    expect(abc.srcName?.toString()).toBe('abc')
    expect(abc.trgName?.toString()).toBe('abc')
    expect(abc.uri?.toString()).toBe('uri://abc')
    expect(abc.uriString).toBe('uri://abc')
  })
  it('updates', () => {
    const nsResolver = NsResolver.create({
      abc: 'uri://abc',
      '': 'uri://uvw',
      xyz: 'uri://xyz',
    })
    const nsResolver1 = nsResolver.forChildTag([{ name: BS.create('abd'), uri: BS.create('uri://abc') }])
    expect(nsResolver1).not.toBe(nsResolver)
    expect(nsResolver1.length).toBe(3)
    const abc1 = nsResolver1[0]
    expect(abc1.srcName && abc1.srcName.toString()).toBe('abd')
    expect(nsResolver1[1] && nsResolver1[1].trgName).not.toBeDefined()
  })
  it('resolves tag name', () => {
    const nsResolver = NsResolver.create({
      abc: 'uri://abc',
      lmn: 'uri://lmn',
      '': 'uri://uvw',
      xyz: 'uri://xyz',
    })
    const nsResolver1 = nsResolver.forChildTag([
      { name: BS.create('abd'), uri: BS.create('uri://abc') },
      { name: undefined, uri: BS.create('uri://lmn') },
      { name: BS.create('uvw'), uri: BS.create('uri://uvw') },
      { name: BS.create('xyz2'), uri: BS.create('uri://xyz') },
      { name: BS.create('ignored'), uri: BS.create('uri://ignored') },
    ])
    const name1 = nsResolver1.resolveTagName(BS.create('abd:tagName1'))
    const name2 = nsResolver1.resolveTagName(BS.create('tagName2'))
    const name3 = nsResolver1.resolveTagName(BS.create('uvw:tagName3'))
    const name4 = nsResolver1.resolveTagName(BS.create('xyz2:tagName4'))
    const name5 = nsResolver1.resolveTagName(BS.create('ignored:tagName5'))
    const name6 = nsResolver1.resolveTagName(BS.create('unknown:tagName6'))
    expect(name1.toString()).toBe('abc:tagName1')
    expect(name2.toString()).toBe('lmn:tagName2')
    expect(name3.toString()).toBe('tagName3')
    expect(name4.toString()).toBe('xyz:tagName4')
    expect(name5.toString()).toBe('ignored:tagName5')
    expect(name6.toString()).toBe('unknown:tagName6')
    const nsResolverDummy = NsResolver.create({})
    expect(nsResolverDummy.resolveTagName(BS.create('tagname')).toString()).toBe('tagname')
  })
  it('resolves attribute names', () => {
    const nsResolver = NsResolver.create({
      abc: 'uri://abc',
      lmn: 'uri://lmn',
      '': 'uri://uvw',
      xyz: 'uri://xyz',
    })
    const nsResolver1 = nsResolver.forChildTag([
      { name: BS.create('abd'), uri: BS.create('uri://abc') },
      { name: undefined, uri: BS.create('uri://lmn') },
      { name: BS.create('uvw'), uri: BS.create('uri://uvw') },
      { name: BS.create('xyz4'), uri: BS.create('uri://xyz') },
    ])
    const [{ name: name1 }, { name: name2 }, { name: name3 }, { name: name4 }] = nsResolver1.resolveAtts([
      { name: BS.create('abd:attname1'), value: BS.create('attvalue1') },
      { name: BS.create('attname2'), value: BS.create('attvalue2') },
      { name: BS.create('uvw:attname3'), value: BS.create('attvalue3') },
      { name: BS.create('xyz4:attname4'), value: BS.create('attvalue4') },
    ])
    expect(name1.toString()).toBe('abc:attname1')
    expect(name2.toString()).toBe('attname2') // att without namespace should not receive a ns
    expect(name3.toString()).toBe('attname3') // att with a removed ns
    expect(name4.toString()).toBe('xyz:attname4')
  })
  it('resolves ns names', () => {
    const nsResolver = NsResolver.create({
      abc: 'uri://abc',
      xyz: 'uri://xyz',
    })
    const nsResolver1 = nsResolver.forChildTag([{ name: BS.create('abd'), uri: BS.create('uri://abc') }])
    const [{ name: name0 }, { name: name1 }] = nsResolver1.resolveNs([
      { name: BS.create('abd'), uri: BS.create('uri://abc') },
      { name: BS.create('lmn'), uri: BS.create('uri://lmn') },
    ])
    expect(name0?.toString()).toBe('abc')
    expect(name1?.toString()).toBe('lmn')
  })
})

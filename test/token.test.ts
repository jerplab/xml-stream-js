import { Token } from '../src/token'
import { BS } from '../src/bs'

describe('Token', () => {
  it(`reads start-tag properties`, () => {
    const startTagA = new Token.StartTag(
      BS.create('a'),
      [],
      [{ name: BS.create('x'), uri: BS.create('//x'), uriString: '//x' }]
    )
    expect(startTagA.tagName).toBe('a')
    expect(startTagA.localName).toBe('a')
    expect(startTagA.namespaceUri).toBeUndefined()
    expect(startTagA.getAttributeNS('//x', 'att1')).toBeUndefined()
    const startTagB = new Token.StartTag(
      BS.create('x:a'),
      [
        { name: BS.create('att'), value: BS.create('val') },
        { name: BS.create('x:att0'), value: BS.create('val0') },
        { name: BS.create('x:att1'), value: BS.create('val1') },
      ],
      [
        { name: undefined, uri: BS.create('//w'), uriString: '//w' },
        { name: BS.create('x'), uri: BS.create('//x'), uriString: '//x' },
      ]
    )
    expect(startTagB.tagName).toBe('x:a')
    expect(startTagB.localName).toBe('a')
    expect(startTagB.namespaceUri).toBe('//x')
    expect(startTagB.getAttributeNS('//x', 'att1')).toBe('val1')
    expect(startTagB.getAttributeNS('//x', 'att2')).toBeUndefined()
    const startTagC = new Token.StartTag(BS.create('c'), [], [])
    expect(startTagC.namespaceUri).toBeUndefined()
  })
  it(`decodes text entties`, () => {
    const xmlText = 'a &amp; &lt; &gt; &quot; &apos; &#97; &#x61;'
    const text = new Token.Text(BS.create(xmlText))
    expect(text.toString()).toBe(xmlText)
    expect(text.textContent).toBe(`a & < > " ' a a`)
    const xmlText2 = 'no entities'
    const text2 = new Token.Text(BS.create(xmlText2))
    expect(text2.toString()).toBe(xmlText2)
    expect(text2.textContent).toBe(xmlText2)
  })
  it(`get content of CDATA`, () => {
    const cdataContent = 'some content'
    const cdata = new Token.CDATA(BS.create('some content'))
    expect(cdata.toString()).toBe(`<![CDATA[${cdataContent}]]>`)
    expect(cdata.textContent).toBe(cdataContent)
  })
  it(`get content of comment`, () => {
    const commentContent = 'some comment'
    const comment = new Token.Comment(BS.create(commentContent))
    expect(comment.toString()).toBe(`<!--${commentContent}-->`)
    expect(comment.textContent).toBe(commentContent)
  })
  it(`throws at unknwon text entties`, () => {
    expect(() => new Token.Text(BS.create('&#abcdef;')).textContent).toThrow()
    expect(() => new Token.Text(BS.create('&#xabcdef;')).textContent).toThrow()
    expect(() => new Token.Text(BS.create('&abcdef;')).textContent).toThrow()
  })
})

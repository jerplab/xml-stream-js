import { BS } from '../src/bs'

describe('Utils Buffers', () => {
  it('compare buffers', () => {
    const b = Buffer.from('xabcy')
    const bs = BS.create(b)
    expect(bs.equals(Buffer.from('xabcy'))).toBeTruthy()
    expect(bs.equals(b)).toBeTruthy()
    expect(bs.equals(Buffer.from('xabcz'))).toBeFalsy()
    expect(bs.equals(Buffer.from('xabcyz'))).toBeFalsy()
  })
  it('isStartOf', () => {
    const bs = BS.create('abc')
    expect(bs.isStartOf(bs, 3)).toBeTruthy()
    expect(bs.isStartOf(BS.create('abcdef'), 3)).toBeTruthy()
  })
  it('encode utf8', () => {
    const bs = BS.create('one €')
    expect(bs.equals(Buffer.from('one €'))).toBeTruthy()
  })
  it('encode empty', () => {
    expect(BS.encode()).toBeDefined()
  })
  it('toString', () => {
    const bs = BS.create('one €')
    expect(bs.toString()).toBe('one €')
    expect(bs.toString(0, 3)).toBe('one')
    expect(bs.toString(4, 4)).toBe('€')
    expect(bs.toString(4)).toBe('€')
  })
  it('peek in debug', () => {
    const bs1 = BS.create('a')
    const bs100 = BS.create('a'.repeat(100))
    expect(bs1._).toBe('a')
    expect(bs100._.length).toBe(51)
  })
  it('decode utf8', () => {
    const bs = BS.create(Buffer.from('one €'))
    expect(bs.toString()).toBe('one €')
  })
})

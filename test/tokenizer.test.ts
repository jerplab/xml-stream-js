import { Tokenizer } from '../src/'
import { Token } from '../src/token'
import { BS } from '../src/bs'

describe('Tokenizer', () => {
  it(`scan complete start tag`, () => {
    const tests: { xml: string; next: string; natts?: number }[] = [
      { xml: '<a>', next: '<a>' },
      { xml: '<a/>', next: '<a/>' },
      { xml: '<a a1="av1">', next: '<a a1="av1">' },
      { xml: "<a a1='av1'>", next: '<a a1="av1">' },
      { xml: '<a a1="av1"/>', next: '<a a1="av1"/>', natts: 1 },
      { xml: '<a xmlns="ns1"/>', next: '<a xmlns="ns1"/>', natts: 0 },
      { xml: '<a XMLNS:x="ns1"/>', next: '<a xmlns:x="ns1"/>' },
      { xml: '<a xmlns:x="ns1"/>', next: '<a xmlns:x="ns1"/>' },
      { xml: '<a  a1   =   "av1"  xmlns:x = "ns1" />', next: '<a a1="av1" xmlns:x="ns1"/>', natts: 1 },
    ]
    for (let { xml, next, natts } of tests) {
      const tokenizer = new Tokenizer()
      const bs = BS.create(xml)
      tokenizer.write(bs)
      const tk = tokenizer.nextToken() as Token.StartTag
      expect(tk).toBeInstanceOf(Token.StartTag)
      expect(tk.toString()).toBe(next)
      expect(tokenizer.exhausted).toBe(true)
      if (natts) expect(tk.atts && tk.atts.length).toBe(natts)
    }
  })
  it(`scan end tag`, () => {
    const tests: { xml: string; next?: string }[] = [
      { xml: '</a>', next: '</a>' },
      { xml: '</a   >', next: '</a>' },
      { xml: '</a ' },
    ]
    for (let { xml, next } of tests) {
      const tokenizer = new Tokenizer()
      const bs = BS.create(xml)
      tokenizer.write(bs)
      const tk = tokenizer.nextToken() as Token.EndTag
      if (next) {
        expect(tk).toBeInstanceOf(Token.EndTag)
        expect(tk.toString()).toBe(next)
      } else {
        expect(tk).toBeUndefined()
      }
      expect(tokenizer.exhausted).toBe(true)
    }
  })
  it(`scan text`, () => {
    const tests: { xml: string; next?: string }[] = [{ xml: 'text <a', next: 'text ' }, { xml: 'text' }]
    for (let { xml, next } of tests) {
      const tokenizer = new Tokenizer()
      const bs = BS.create(xml)
      tokenizer.write(bs)
      const tk = tokenizer.nextToken()
      if (next) {
        expect(tk).toBeInstanceOf(Token.Text)
        expect(tk && tk.toString()).toBe(next)
      } else {
        expect(tk).toBeUndefined()
      }
    }
  })
  it(`scan text entity`, () => {
    const tests: { xml: string; next?: string }[] = [{ xml: 'text &amp; <', next: 'text & ' }, { xml: 'text' }]
    for (let { xml, next } of tests) {
      const tokenizer = new Tokenizer()
      const bs = BS.create(xml)
      tokenizer.write(bs)
      const tk = tokenizer.nextToken() as Token.Text
      if (next) {
        expect(tk).toBeInstanceOf(Token.Text)
        expect(tk && tk.textContent).toBe(next)
      } else {
        expect(tk).toBeUndefined()
      }
    }
  })
  it(`scan comment`, () => {
    const tests: { xml: string; next?: string }[] = [
      { xml: '<!-- a comment -->', next: '<!-- a comment -->' },
      { xml: '<!-- a comment -- more -->', next: '<!-- a comment -- more -->' },
    ]
    for (let { xml, next } of tests) {
      const tokenizer = new Tokenizer()
      const bs = BS.create(xml)
      tokenizer.write(bs)
      const tk = tokenizer.nextToken()
      if (next) {
        expect(tk).toBeInstanceOf(Token.Comment)
        expect(tk && tk.toString()).toBe(next)
      } else {
        expect(tk).toBeUndefined()
      }
      expect(tokenizer.exhausted).toBe(true)
    }
  })
  it(`itterates`, () => {
    const tokenizer = new Tokenizer()
    const bs = BS.create('<a><b/><c></c><d/></a>')
    tokenizer.write(bs)
    const tka = tokenizer.nextToken()
    expect(tka).toBeInstanceOf(Token.StartTag)
    const tkb = tokenizer.nextToken() as Token.StartTag
    expect(tkb).toBeInstanceOf(Token.StartTag)
    expect(tkb.selfClosing).toBe(true)
    const tkc = tokenizer.nextToken()
    expect(tkc).toBeInstanceOf(Token.StartTag)
    const tkce = tokenizer.nextToken()
    expect(tkce).toBeInstanceOf(Token.EndTag)
    const tkd = tokenizer.nextToken() as Token.StartTag
    expect(tkd).toBeInstanceOf(Token.StartTag)
    expect(tkd.selfClosing).toBe(true)
    const tkae = tokenizer.nextToken()
    expect(tkae).toBeInstanceOf(Token.EndTag)
    expect(tokenizer.exhausted).toBe(true)
  })
  it(`resolves ns map`, () => {
    const tokenizer = new Tokenizer({ x: '//urix', y: '//uriy', '': '//uriz' })
    const bs = BS.create('<a a1="av1" xmlns="//urix" xmlns:y0="//uriy" xmlns:z0="//uriz"><y0:b/><z0:c/>')
    tokenizer.write(bs)
    const a = tokenizer.nextToken() as Token.StartTag
    expect(a).toBeInstanceOf(Token.StartTag)
    expect(a.toString()).toBe('<x:a a1="av1" xmlns:x="//urix" xmlns:y="//uriy" xmlns="//uriz">')
    const b = tokenizer.nextToken() as Token.StartTag
    expect(b).toBeInstanceOf(Token.StartTag)
    expect(b.toString()).toBe('<y:b/>')
    const c = tokenizer.nextToken() as Token.StartTag
    expect(c).toBeInstanceOf(Token.StartTag)
    expect(c.toString()).toBe('<c/>')
  })
  it(`gets attributes`, () => {
    const tokenizer = new Tokenizer()
    const bs = BS.create('<a a1="av€" x:a1="av1" xmlns:x="//urix"/>')
    tokenizer.write(bs)
    const a = tokenizer.nextToken() as Token.StartTag
    expect(a).toBeInstanceOf(Token.StartTag)
    expect(a.getAttribute('a1')).toBe('av€')
    expect(a.getAttributeNS('//urix', 'a1')).toBe('av1')
    expect(a.getAttributeNS('//uriy', 'a1')).toBe('av€')
  })
  it(`skipping`, () => {
    const tokenizer = new Tokenizer()
    const bs = BS.create('<a><b><c/>some<d>inner</d>text</b><b>some text</b><b>more text</b></a>')
    tokenizer.write(bs)
    const a = tokenizer.nextToken()
    expect(a).toBeInstanceOf(Token.StartTag)
    const b0 = tokenizer.nextToken()
    expect(b0).toBeInstanceOf(Token.StartTag)
    tokenizer.skipChildNodes()
    const b0e = tokenizer.nextToken() as Token.EndTag
    // the next token should be the end-tag of first element 'b'
    expect(b0e).toBeInstanceOf(Token.EndTag)
    expect(b0e.name.toString()).toBe('b')
    const b1 = tokenizer.nextToken()
    expect(b1).toBeInstanceOf(Token.StartTag)
    tokenizer.skipChildNodes()
    const b1e = tokenizer.nextToken() as Token.EndTag
    expect(b1e).toBeInstanceOf(Token.EndTag)
    expect(b1e.name.toString()).toBe('b')
    const b2 = tokenizer.nextToken()
    expect(b2).toBeInstanceOf(Token.StartTag)
    const b2t = tokenizer.nextToken() as Token.Text
    // the next token should be the text of the 3d element 'b'
    expect(b2t).toBeInstanceOf(Token.Text)
    const b2e = tokenizer.nextToken() as Token.EndTag
    expect(b1e).toBeInstanceOf(Token.EndTag)
    expect(b2e.name.toString()).toBe('b')
  })
  it(`skipping 2`, () => {
    const tokenizer = new Tokenizer()
    const bs = BS.create('<root id="a0">some text a0<b>some</b><a>some text ab0</a></root>')
    tokenizer.write(bs)
    const root = tokenizer.nextToken()
    expect(root).toBeInstanceOf(Token.StartTag)
    const t = tokenizer.nextToken()
    expect(t).toBeInstanceOf(Token.Text)
    const b = tokenizer.nextToken()
    expect(b).toBeInstanceOf(Token.StartTag)
    tokenizer.skipChildNodes(true)
    const a = tokenizer.nextToken() as Token.StartTag
    expect(a).toBeInstanceOf(Token.StartTag)
    expect(a.name.toString()).toBe('a')
  })
  it(`skips declaration`, () => {
    const tokenizer = new Tokenizer()
    const bs = BS.create('<?xml encoding="utf8"?><?somedecl q="?"?><a><b><c/>some<d>inner</d>text</b></a>')
    tokenizer.write(bs)
    const a = tokenizer.nextToken()
    expect(a).toBeInstanceOf(Token.StartTag)
  })
  it(`scan CDATA`, () => {
    const tests: { xml: string; next?: string }[] = [
      { xml: '<![CDATA[some stuff]]>', next: '<![CDATA[some stuff]]>' },
      { xml: '<![CDATA[some stuff]]' },
      { xml: '<![CDATA[  some stuff  ] ]]  ++  ]]>', next: '<![CDATA[  some stuff  ] ]]  ++  ]]>' },
      { xml: '<![CDATA[  some stuff  ] ]]]]>', next: '<![CDATA[  some stuff  ] ]]]]>' },
      { xml: '<![CDATA[  some stuff  ]] ]]]>', next: '<![CDATA[  some stuff  ]] ]]]>' },
      { xml: '<![CDATA[  some stuff  ]]] ]]>', next: '<![CDATA[  some stuff  ]]] ]]>' },
      { xml: '<![CDATA[  some stuff  ]]]]]>', next: '<![CDATA[  some stuff  ]]]]]>' },
      { xml: '<![CDATA[]]>', next: '<![CDATA[]]>' },
    ]
    for (let { xml, next } of tests) {
      const tokenizer = new Tokenizer()
      const bs = BS.create(xml)
      tokenizer.write(bs)
      const tk = tokenizer.nextToken()
      if (next) {
        expect(tk).toBeInstanceOf(Token.CDATA)
        expect(tk && tk.toString()).toBe(next)
      } else {
        expect(tk).toBeUndefined()
      }
      expect(tokenizer.exhausted).toBe(true)
    }
  })
  it(`scans chunks `, () => {
    const xmlIn =
      '<?xml version="1.0" encoding="UTF-8"?><root><childb b1="bv1"/><childc   c1  =  "cv1"  > some text </childc  ><childd  /><e  skippingChildren="true">ignoringToken.Text<ignore/></e><![CDATA[  some stuff  ]  ++  ]]><![CDATA[]]><!-- a comment --></root  >'
    const expecting =
      '<root><childb b1="bv1"/><childc c1="cv1"> some text </childc><childd/><e skippingChildren="true"></e><![CDATA[  some stuff  ]  ++  ]]><![CDATA[]]><!-- a comment --></root>'
    const bs = BS.create(xmlIn)
    bs.forEach((_, i) => {
      // i = 167
      const tokenizer = new Tokenizer()
      const tokens: Token.Token[] = []
      let token: Token.Token | undefined
      tokenizer.write(bs.slice(0, i))
      try {
        while ((token = tokenizer.nextToken())) {
          if (token instanceof Token.StartTag && token.getAttribute('skippingChildren') === 'true') {
            tokenizer.skipChildNodes()
          }
          tokens.push(token)
        }
        tokenizer.write(bs.slice(i))
        while ((token = tokenizer.nextToken())) {
          if (token instanceof Token.StartTag && token.getAttribute('skippingChildren') === 'true') {
            tokenizer.skipChildNodes()
          }
          tokens.push(token)
        }
      } catch (e) {
        debugger
      }
      const xmlOut = tokens.join('')
      if (expecting !== xmlOut) debugger
      expect(xmlOut).toBe(expecting)
    })
  })
  describe('Readme examples', () => {
    it(`tokinizes first example`, () => {
      const tokenizer = new Tokenizer()
      const tokens: Token.Token[] = []
      let token: Token.Token | undefined
      try {
        tokenizer.write('<a><b b1="value b1"><c/>some<d>inn')
        while ((token = tokenizer.nextToken())) {
          tokens.push(token)
        }
        tokenizer.write('er</d>text</b></a>')
        while ((token = tokenizer.nextToken())) {
          tokens.push(token)
        }
      } catch (e) {
        // will not happen in this case
      }
      const aTag = tokens[0] as Token.StartTag
      expect(aTag.tagName === 'a').toBe(true)
      const bTag = tokens[1] as Token.StartTag
      expect(bTag.getAttribute('b1') === 'value b1').toBe(true)
      const text = tokens[7] as Token.Text
      text.toString() === 'text'
      expect(tokens.join('')).toBe('<a><b b1="value b1"><c/>some<d>inner</d>text</b></a>')
      expect(tokenizer.exhausted).toBe(true)
    })
  })
  describe('Tokenizer fixes', () => {
    it(`fixes unclosed tags`, () => {
      const bs = BS.create('<a><b></a>')
      const tokenizer = new Tokenizer()
      tokenizer.write(bs)
      const tokens: Token.Token[] = []
      let token: Token.Token | undefined
      while ((token = tokenizer.nextToken())) {
        tokens.push(token)
      }
      const xmlOut = tokens.join('')
      expect(xmlOut).toBe('<a><b></a>')
    })
  })
  describe('Tokenizer namespace resolver', () => {
    it(`resolves ns for tag name`, () => {
      const bs = BS.create('<x:a XMLNS:x="ns1"></x:a>')
      const tokenizer = new Tokenizer({ y: 'ns1' })
      tokenizer.write(bs)
      const tokens: Token.Token[] = []
      let token: Token.Token | undefined
      while ((token = tokenizer.nextToken())) {
        tokens.push(token)
      }
      const xmlOut = tokens.join('')
      expect(xmlOut).toBe('<y:a xmlns:y="ns1"></y:a>')
    })
  })
  describe('Tokenizer errors', () => {
    it(`throws at tag with no name`, () => {
      const bs = BS.create('</>')
      const tokenizer = new Tokenizer()
      tokenizer.write(bs)
      expect(() => tokenizer.nextToken()).toThrow()
    })
    it(`throws at missing attribute value`, () => {
      const bs = BS.create('<a a1/>')
      const tokenizer = new Tokenizer()
      tokenizer.write(bs)
      expect(() => tokenizer.nextToken()).toThrow()
    })
    it(`throws at missing attribute value (2)`, () => {
      const bs = BS.create('<a a1=/>')
      const tokenizer = new Tokenizer()
      tokenizer.write(bs)
      expect(() => tokenizer.nextToken()).toThrow()
    })
    it(`throws at unclosed tag`, () => {
      const bs = BS.create('<a/<b/>')
      const tokenizer = new Tokenizer()
      tokenizer.write(bs)
      expect(() => tokenizer.nextToken()).toThrow()
    })
    it(`throws at unclosed end tag`, () => {
      const bs = BS.create('</a a1="E">')
      const tokenizer = new Tokenizer()
      tokenizer.write(bs)
      expect(() => tokenizer.nextToken()).toThrow()
    })
    it(`throws at bad CDATA start`, () => {
      const tokenizer = new Tokenizer()
      tokenizer.write(BS.create('<![ some stuff]]>'))
      expect(() => tokenizer.nextToken()).toThrow()
    })
    it(`throws at bad CDATA start`, () => {
      const tokenizer = new Tokenizer()
      tokenizer.write(BS.create('<![C some stuff]]>'))
      expect(() => tokenizer.nextToken()).toThrow()
    })
    it(`throws at bad CDATA start`, () => {
      const tokenizer = new Tokenizer()
      tokenizer.write(BS.create('<![CD some stuff]]>'))
      expect(() => tokenizer.nextToken()).toThrow()
    })
    it(`throws at bad CDATA start`, () => {
      const tokenizer = new Tokenizer()
      tokenizer.write(BS.create('<![CDA some stuff]]>'))
      expect(() => tokenizer.nextToken()).toThrow()
    })
    it(`throws at bad CDATA start`, () => {
      const tokenizer = new Tokenizer()
      tokenizer.write(BS.create('<![CDAT some stuff]]>'))
      expect(() => tokenizer.nextToken()).toThrow()
    })
    it(`throws at bad CDATA start`, () => {
      const tokenizer = new Tokenizer()
      tokenizer.write(BS.create('<![CDATA some stuff]]>'))
      expect(() => tokenizer.nextToken()).toThrow()
    })
    it(`throws at bad comment start`, () => {
      const bs = BS.create('<!- some comment-->')
      const tokenizer = new Tokenizer()
      tokenizer.write(bs)
      expect(() => tokenizer.nextToken()).toThrow()
    })
    it(`throws at unsupported tag`, () => {
      const bs = BS.create('<!SOMETAG some tag...')
      const tokenizer = new Tokenizer()
      tokenizer.write(bs)
      expect(() => tokenizer.nextToken()).toThrow()
    })
  })
})
